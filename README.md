# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

*Crossy Rogue is a realtime, hack and slash roguelike where you play a character armed with only a torch to ward off the encroaching darkness, and a weapon to ward off the nasty things that live in said darkness.

### How do I get set up? ###

This game is being made in Unity 5, and currently is being targetted towards PC/OSX deployment. To check out our progress, simply pull the repository, and open the project in Unity.